package dto;

public class Raices {

	// Atributos
	protected double a;
	protected double b;
	protected double c;

	// Constructor por defecto
	public Raices(double a, double b, double c) {
		
		this.a = a;
		this.b = b;
		this.c = c;
		
	}

	// Metodo para obtener las dos soluciones posibles de la operacion
	public void obtenerRaices() {
		
		double s1 = 0;
		double s2 = 0;
		
		s1 = (- b + Math.sqrt(Math.pow(b, 2) - (4*a*c))) / (2*a);
		s2 = (- b - Math.sqrt(Math.pow(b, 2)-(4*a*c)))/(2*a);
		
		System.out.println("Primera solucion: ");
		System.out.println(s1);
		System.out.println("");
		System.out.println("Segunda solucion: ");
		System.out.println(s2);
		
    }
	
	// Metodo para obtener la solucion de la operacion
	public void obtenerRaiz() {
		
        double s = 0;
        
        s = (-b) / (2 * a);
        
        System.out.println("Solucion: ");
        System.out.println(s);
        
    }
	
	// Metodo que calcula el discriminante de los valores introducidos
	public double getDiscriminante() {
		
        double discriminante = 0;
        
        discriminante = Math.pow(b, 2) - (4 * a * c);
        
        return discriminante;
        
    }

	// Metodo que determina si existen dos soluciones
	public boolean tieneRaices() {
		
        if (getDiscriminante() > 0) {
        	
        	return true;
        	
        }
    	
        return false;
        
    }
	
	// Metodo que determina si existe una unica solucion
	public boolean tieneRaiz() {
		
        if (getDiscriminante() == 0) {
        	
        	return true;
        	
        }
    	
        return false;
        
    }
	
	// Metodo que segun si existen una o dos soluciones, llama al metodo de obtencion de uno o dos soluciones
	// Si no hay solucion tambien lo indica
	public void calcular() {
		
        if (tieneRaiz()) {
        	
        	obtenerRaiz();
        	
        } else if (tieneRaices()) {
        	
        	obtenerRaices();
        	
        } else {
        	
        	System.out.println("No hay solucion");
        	
        }
        
    }
	
}
