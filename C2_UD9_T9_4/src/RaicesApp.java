import dto.Raices;

public class RaicesApp {

	public static void main(String[] args) {
		
		// Creamos un objeto al que le pasamos 3 coeficientes
		Raices operacion = new Raices(21, 42, 12);
		
		// Llamamos al metodo calcular al que le pasamos el objeto recien creado
		operacion.calcular();
		
	}
	
}
